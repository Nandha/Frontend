    var tasks = [],
        task, lists = [{ name: "Inbox", tasks: [] }, { name: "Today", tasks: [] }],
        list = lists[0];
    var emptyTask = { text: "Note making App", subtasks: [], starred: false, dueDate: "Today", completed: false };

    function addSubTask() {
        var subtask = getDOMById("subtask-input")
        var subtasks = getDOMById("subtasks");
        subtasks.value = "";
        if (subtask.value === "") return;
        if (task.subtasks === null) return;
        task.subtasks.push(subtask.value);
        subtask.value = "";
        task.subtasks.forEach(function(sub, index) {
            subtasks.value += index + 1 + ". " + sub + "\n";
        });
        console.log("+++++++" + task.subtasks);
    }

    function getDOMById(id) {
        return document.getElementById(id);
    }

    function getDOMByClass(className, index) {
        if ((null !== className) && (null !== index)) {
            return document.getElementsByClassName(className)[index];
        }
    }

    function createElement(element) {
        var newElement = document.createElement(element.name);
        var attributes = Object.keys(element.properties);
        attributes.forEach(function(attribute) {
            newElement[attribute] = element.properties[attribute];
        });
        return newElement;
    }

    function createTask() {
        var taskText = getDOMById("task");
        if (taskText.value === "") return;
        createTaskDiv(taskText.value, tasks.length);
        tasks.push({ text: taskText.value, subtasks: [], starred: false, dueDate: Date(), completed: false });
        taskText.value = "";
    }

    function addEventForCheckbox(event) {
        if (tasks[event.target.index].completed === false) {
            tasks[event.target.index].completed = true;
            console.log("&&&" + (event.target.parentNode.childNodes[2].className = "item-display strike"));
        } else {
            tasks[event.target.index].completed = false;
            console.log("&&&" + (event.target.parentNode.childNodes[2].className = "item-display"));
        }
    }

    function addEventForStar(event) {
        if (tasks[event.target.index].starred === false) {
            event.target.className = "fa fa-star fa-lg star-style dark";
            tasks[event.target.index].starred = true;
            console.log(list);
            console.log(tasks[event.target.index]);
        } else {
            event.target.className = "fa fa-star fa-lg star-style";
            tasks[event.target.index].starred = false;
            console.log(tasks[event.target.index]);
        }
    }

    function appendChild(appendTo, elements) {
        elements.forEach(function(element) {
            appendTo.appendChild(element);
        });
    }

    function createTaskDiv(taskText, newIndex, starred, completed) {
        var taskDiv = createElement({ name: "div", properties: { id: "item", className: "item-style", index: newIndex } });
        var taskSpan = createElement({ name: "span", properties: { className: "item-display" } });
        var checkbox = createElement({ name: "input", properties: { type: "checkbox", className: "item-check-box item-display", index: newIndex } });
        checkbox.addEventListener('click', function(event) { addEventForCheckbox(event) });
        var text = createElement({ name: "p", properties: { className: ((starred !== 'undefined') && (starred === true)) ? "item-display strike" : "item-display", innerHTML: taskText } });
        var mark = createElement({ name: "i", properties: { className: ((starred !== 'undefined') && (starred === true)) ? "fa fa-star fa-lg star-style dark" : "fa fa-star fa-lg star-style", index: newIndex } });
        mark.addEventListener('click', function(event) {
            addEventForStar(event);
        });
        var dueDate = createElement({ name: "p", properties: { className: "due-style", innerHTML: Date().substr(0, 10) } });
        appendChild(taskSpan, [checkbox, mark, text, dueDate]);
        appendChild(taskDiv, [taskSpan]);
        taskDiv.addEventListener('dblclick', function(event) {
            task = tasks[event.target.index];
            showTask();
        }, true);
        getDOMById("items").appendChild(taskDiv);
    }

    function showTask() {
        getDOMById("subStar").className = "star-display fa fa-star fa-lg";
        if (task.starred === true) {
            getDOMById("subStar").className = "star-display fa fa-star fa-lg dark";
        }
        getDOMByClass("title", 0).innerHTML = task.text;
        if (task.completed === true) {
            getDOMByClass("title", 0).innerHTML = task.text.strike();
        }
        getDOMById("dueDateText").innerHTML = task.dueDate;
        document.getElementById("subtasks").value = task.subtasks;
    }

    function deleteTask() {
        var possition = tasks.indexOf(task);
        getDOMById("items").innerHTML = "";
        console.log(tasks, possition);
        tasks.splice(possition, 1);
        tasks.forEach(function(task, index) {
            createTaskDiv(task.text, index, task.starred, task.completed);
        });
    }

    function addEventForList(e) {
        list = lists[e.target.index];
        tasks = list.tasks;
        getDOMById("task").placeholder = "Add an item in " + list.name + " ....";
        getDOMById("items").innerHTML = "";
        tasks.forEach(function(task, index) {
            createTaskDiv(task.text, index, task.starred, task.completed);
        });
    }

    function createList() {
        var listText = getDOMById("list");
        if (listText.value === undefined) return;
        console.log(list.value);
        var listelement = createElement({ name: "a", properties: { index: lists.length, className: "list-group-item dotted-line txt-bold", href: "#" } })
        var listIcon = createElement({ name: "i", properties: { className: "fa fa fa-bars fa-lg" } });
        var text = createElement({ name: "span", properties: { className: "group-display" } });
        text.innerHTML = listText.value;
        listelement.addEventListener("click", function(e) {
            addEventForList(e);
        });
        appendChild(listelement, [listIcon, text]);
        appendChild(getDOMById("side-list"), [listelement]);
        lists.push({ name: listText.value, tasks: [] });
        console.log(lists);
        listText.value = "";
    }

    document.getElementById('task').addEventListener('keydown', function(event) {
        if (event.keyCode == 13) {
            createTask();
        }
    });

    document.getElementById('subtask').addEventListener('click', function(event) {
        addSubTask();
    });

    document.getElementById('addlist').addEventListener('click', function(event) {
        createList();
    });

    document.getElementById('delete').addEventListener('click', function(event) {
        if (confirm("Do you want to delete?")) {
            deleteTask();
            task = emptyTask;
            showTask()
        }
    });